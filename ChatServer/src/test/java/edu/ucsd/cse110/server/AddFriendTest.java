package edu.ucsd.cse110.server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AddFriendTest {

	@Test
	public void testAddFriend() {
		AddFriend friend = new AddFriend("User1", "User2");
		assertEquals("User1", friend.getUser1());
		assertEquals("User2", friend.getUser2());
	}

	@Test
	public void testGetUser1() {
		AddFriend friend = new AddFriend("User1", "User2");
		assertEquals("User1", friend.getUser1());
	}

	@Test
	public void testSetUser1() {
		AddFriend friend = new AddFriend("User1", "User2");
		friend.setUser1("NewUser1");
		assertEquals("NewUser1",friend.getUser1());
	}

	@Test
	public void testGetUser2() {
		AddFriend friend = new AddFriend("User1", "User2");
		assertEquals("User2", friend.getUser2());
	}

	@Test
	public void testSetUser2() {
		AddFriend friend = new AddFriend("User1", "User2");
		friend.setUser2("NewUser2");
		assertEquals("NewUser2",friend.getUser2());
	}

}
