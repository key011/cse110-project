package edu.ucsd.cse110.server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SendToUserTest {
	SendToUser send;
	@Before
	public void setUp() throws Exception {
		send = new SendToUser("me", "message");
	}

	@After
	public void tearDown() throws Exception {
		send = null;
	}

	@Test
	public void testSendToUser() {
		assertEquals("me",send.getRecipient());
		assertEquals("message", send.getMsg());
	}

	@Test
	public void testGetRecipient() {
		assertEquals("me",send.getRecipient());
	}

	@Test
	public void testSetRecipient() {
		send.setRecipient("not me");
		assertEquals("not me", send.getRecipient());
	}

	@Test
	public void testGetMsg() {
		assertEquals("message", send.getMsg());
	}

	@Test
	public void testSetMsg() {
		send.setMsg("new message");
		assertEquals("new message", send.getMsg());
	}

}
