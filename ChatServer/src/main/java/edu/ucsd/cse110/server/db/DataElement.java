package edu.ucsd.cse110.server.db;

import java.io.Serializable;

import javax.jms.Queue;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class DataElement implements Serializable {

	private static final long serialVersionUID = 1L;
	private Queue queue;
	private String name;
	
	public DataElement() {	}
	
	public DataElement(String name, Queue queue) {
		this.name = name;
		this.queue=queue;
	}	
	@Id @GeneratedValue
	public Integer getId() { 
		return id; 
	}
	public void setId(Integer id) { 
		this.id = id; 
	}
	private Integer id;
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
	@Column(name="Queue")
	public Queue getQueue() {
		return queue;
	}
	public void setQueue(Queue queue) {
		this.queue = queue;
	}	

	
	//use serialiaz

	@Override
	public String toString() {
		return "DataElement [id=" + id + ", name=" + name + ", queue="
				+ queue + "]";
	}

}
