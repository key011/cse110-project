package edu.ucsd.cse110.server.db;

import org.springframework.data.repository.CrudRepository;


public interface UserQueueRepository extends CrudRepository<UserQueue, Integer> {
	public Iterable<UserQueue> findByName(String name);
}
