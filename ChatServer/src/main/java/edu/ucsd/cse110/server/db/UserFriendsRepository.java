package edu.ucsd.cse110.server.db;

import org.springframework.data.repository.CrudRepository;


public interface UserFriendsRepository extends CrudRepository<UserFriends, Integer> {
	public Iterable<UserFriends> findByName(String name);
}
