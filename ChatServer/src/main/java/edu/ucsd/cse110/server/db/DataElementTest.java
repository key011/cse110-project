package edu.ucsd.cse110.server.db;

import static org.junit.Assert.*;

import javax.jms.Queue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DataElementTest {
	private DataElement de;
	private Queue queue;
	
	@Before
	public void setUp() throws Exception {
		de = new DataElement("Data", queue);
		de.setId(1);
	}

	@After
	public void tearDown() throws Exception {
		de = null;
	}

	@Test
	public void testGetId() {
		assertEquals(de.getId(), id);
	}

	@Test
	public void testSetId() {
		assertEquals(de.getId(), id);
	}

	@Test
	public void testGetName() {
		assertEquals(de.getName(), "Data");
	}

	@Test
	public void testSetName() {
		de.setName("Database");
		assertEquals(de.getName(), "Database");
	}

	@Test
	public void testGetQueue() {
		assertEquals( 1, 1 );
	}

	@Test
	public void testSetQueue() {
		assertEquals( 1, 1 );
	}

	@Test
	public void testDataElement() {
		assertEquals( 1, 1 );
	}

	@Test
	public void testDataElementStringQueue() {
		assertEquals( 1, 1 );
	}

	@Test
	public void testToString() {
		assertEquals( 1, 1 );
	}

}
