package edu.ucsd.cse110.server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserTest {
	User user;
	@Before
	public void setUp() throws Exception {
		user = new User("usr");
	}

	@After
	public void tearDown() throws Exception {
		user = null;
	}

	@Test
	public void testGetName() {
		user.setName("me");
		assertEquals("me", user.getName());
		
	}

	@Test
	public void testSetName() {
		user.setName("me");
		assertEquals("me", user.getName());
	}

	@Test
	public void testUser() {
		assertEquals("usr", user.getUsername());
	}

}
