package edu.ucsd.cse110.server;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import edu.ucsd.cse110.server.db.DataElement;
import edu.ucsd.cse110.server.db.DataElementRepository;

public class User implements Serializable{
	private String username;
	private Object [] friendlist = new String[20];
	private String name;
	
	public User(String username)
	{
		this.username=username;
	}
	public String getUsername() {
		return username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String toString() {
		return "User [ " + "username=" + username + "]";
	}
}
