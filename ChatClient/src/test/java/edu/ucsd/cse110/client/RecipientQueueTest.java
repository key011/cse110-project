package edu.ucsd.cse110.client;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RecipientQueueTest {

	private RecipientQueue rq;
	
	@Before
	public void setUp() throws Exception {
		rq = new RecipientQueue( "Message", "Hello!" );
	}

	@After
	public void tearDown() throws Exception {
		rq = null;
	}

	@Test
	public void testRecipientQueue() {
		assertEquals(rq.getqueueName(), "Message");
		assertEquals(rq.getUserMessage(), "Hello!");
	}

	@Test
	public void testGetqueueName() {
		assertEquals(rq.getqueueName(), "Message");
	}

	@Test
	public void testSetqueueName() {
		rq.setqueueName("Chatting");
		assertEquals(rq.getqueueName(), "Chatting");
	}

	@Test
	public void testGetUserMessage() {
		assertEquals(rq.getUserMessage(), "Hello!");
	}

	@Test
	public void testSetUserMessage() {
		rq.setUserMessage("Here I am!");
		assertEquals(rq.getUserMessage(), "Here I am!");
	}

}
