package edu.ucsd.cse110.client;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RegisterUserTest {
	private RegisterUser ru;
	@Before
	public void setUp() throws Exception {
		ru = new RegisterUser();
	}

	@After
	public void tearDown() throws Exception {
		ru = null;
	}

	@Test
	public void testGetUsername() {
		ru.setUsername("user1");
		assertEquals(ru.getUsername(), "user1");
	}

	@Test
	public void testGetPassword() {
		ru.setPassword("password");
		assertEquals(ru.getPassword(), "password");
	}

	@Test
	public void testSetUsername() {
		ru.setUsername("user1");
		assertEquals(ru.getUsername(), "user1");
	}

	@Test
	public void testSetPassword() {
		ru.setPassword("password");
		assertEquals(ru.getPassword(), "password");
	}

	@Test
	public void testPromptUsername() {
		ru.setUsername("user1");
		assertEquals(ru.getUsername(), "user1");
	}

	@Test
	public void testPromptPassword() {
		ru.setPassword("password");
		assertEquals(ru.getPassword(), "password");
	}

}
