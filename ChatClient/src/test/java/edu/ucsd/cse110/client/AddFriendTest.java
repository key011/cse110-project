package edu.ucsd.cse110.client;

import static org.junit.Assert.*;

import org.junit.Test;

public class AddFriendTest {

	@Test
	public void testAddFriend() {
		AddFriend friend = new AddFriend("friend1","friend2");
		assertEquals(friend.getUser1(),"friend1");
	}
	@Test
	public void testAddFriend2() {
		AddFriend friend = new AddFriend("friend1","friend2");
		assertEquals(friend.getUser2(),"friend2");
	}
	

}
