package edu.ucsd.cse110.client;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SendToUserTest {
	private SendToUser stu;
	
	@Before
	public void setUp() throws Exception {
		stu = new SendToUser("user", "message");
	}

	@After
	public void tearDown() throws Exception {
		stu = null;
	}

	@Test
	public void testSendToUser() {
		assertEquals(stu.getRecipientUserName(), "user");
		assertEquals(stu.getMsg(), "message");
	}

	@Test
	public void testGetRecipientUserName() {
		assertEquals(stu.getRecipientUserName(), "user");
	}

	@Test
	public void testSetRecipientUserName() {
		stu.setRecipientUserName("username");
		assertEquals(stu.getRecipientUserName(), "username");
	}

	@Test
	public void testGetMsg() {
		assertEquals(stu.getMsg(), "message");
	}

	@Test
	public void testSetMsg() {
		stu.setMsg("Meep!");
		assertEquals(stu.getMsg(), "Meep!");
	}

}
