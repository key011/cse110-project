package edu.ucsd.cse110.client;

import static org.junit.Assert.*;

import javax.jms.MessageProducer;
import javax.jms.Session;

import org.junit.Test;

public class ChatClientTest {
	private MessageProducer producer;
	private Session session;
	private String username;
	
	@Test
	public void testGetMessageProducer() { 
		ChatClient chat = new ChatClient(producer,session);
		assertEquals(chat.getMessageProducer(),"producer");
	}
	@Test
	public void testGetSession() {
		ChatClient chat = new ChatClient(producer,session);
		assertEquals(chat.getSession(),"session");
	}
	@Test
	public void testGetUsername() {
		ChatClient chat = new ChatClient(producer,session);
		assertEquals(chat.getUsername(),"User1");
	}

}
