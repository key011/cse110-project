package edu.ucsd.cse110.client;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RequestForRecipientQueueTest {

	private RequestForRecipientQueue rq;
	@Before
	public void setUp() throws Exception {
		rq = new RequestForRecipientQueue("user");
	}

	@After
	public void tearDown() throws Exception {
		rq = null;
	}

	@Test
	public void testRequestForRecipientQueue() {
		assertEquals(rq.getRecipientUserName(), "user");
	}

	@Test
	public void testGetRecipientUserName() {
		assertEquals(rq.getRecipientUserName(), "user");
	}

	@Test
	public void testSetRecipientUserName() {
		rq.setRecipientUserName("person");
		assertEquals(rq.getRecipientUserName(), "person");
	}

	@Test
	public void testGetUserMessage() {
		assertEquals(rq.getUserMessage(), "");
	}

	@Test
	public void testSetUserMessage() {
		rq.setUserMessage("Hello guys!");
		assertEquals(rq.getUserMessage(), "Hello guys!");
	}

}
