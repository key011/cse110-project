package edu.ucsd.cse110.client;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

public class ChatClient {
	private MessageProducer producer;
	private MessageConsumer consumer;
	private Session session;
	private String username;
	private RecipientQueue recipientQueue;
	private Queue temporaryQueue;
	
	public ChatClient(MessageProducer producer, MessageConsumer consumer, Session session,Queue temporaryQueue) {
		super();
		this.producer = producer;
		this.consumer = consumer;
		this.session = session;
		this.temporaryQueue=temporaryQueue;
	}
	
	public void send(String msg) throws JMSException {
		producer.send(session.createTextMessage(msg));
	}
	
	public void send(SendToUser stu) throws JMSException {
		producer.send(session.createObjectMessage(stu));
	}
	
	public void send(SendQueueName stu) throws JMSException {
		producer.send(session.createObjectMessage(stu));
	}
	
	
	

	
	//public void send(TemporarySessionQueue tsq) throws JMSException {
	//	producer.send(session.createObjectMessage(tsq));
	//}
	
	public RecipientQueue getRecipientQueue() {
		return recipientQueue;
	}
	public MessageProducer getMessageProducer()
	{
		return producer;
	}
	public String getUsername() {
		return username;
	}
	public Session getSession() {
		return session;
	}
	
	
	
	public void setRecipientQueue(RecipientQueue recipientQueue) {
		this.recipientQueue = recipientQueue;
	}
	//	public void send(SendToUser msg) throws JMSException {
	//	 producer.send(session.createObjectMessage(msg));
	//	}
	//
	//	public void send(AddFriend add) throws JMSException {
	//	 producer.send(session.createObjectMessage(add));
	//
	//	}
	//

	public MessageProducer getProducer() {
		return producer;
	}

	public void setProducer(MessageProducer producer) {
		this.producer = producer;
	}

	public Queue getTemporaryQueue() {
		return temporaryQueue;
	}

	public void setTemporaryQueueName(Queue temporaryQueue) {
		this.temporaryQueue = temporaryQueue;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public MessageConsumer getConsumer() {
		return consumer;
	}

	public void setConsumer(MessageConsumer consumer) {
		this.consumer = consumer;
	}

	public void setTemporaryQueue(Queue temporaryQueue) {
		this.temporaryQueue = temporaryQueue;
	}
}