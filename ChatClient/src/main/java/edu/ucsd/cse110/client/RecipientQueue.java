package edu.ucsd.cse110.client;

import java.io.Serializable;
import javax.jms.Queue;

public class RecipientQueue implements Serializable {
	private String queueName;
	private String userMessage;
	
	public RecipientQueue(String queueName, String userMessage) {
		this.queueName = queueName;
		this.userMessage = userMessage;
	}
	
	public String getqueueName() {
		return queueName;
	}
	
	public void setqueueName(String queueName) {
		this.queueName = queueName;
	}
	
	public String getUserMessage() {
		return userMessage;
	}
	
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}
}