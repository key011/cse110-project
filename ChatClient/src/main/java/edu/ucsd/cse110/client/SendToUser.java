package edu.ucsd.cse110.client;

import java.io.Serializable;

public class SendToUser implements Serializable{
	private String recipient;
	private String msg;
	
	public SendToUser(String recipient, String msg) {
		super();
		this.recipient = recipient;
		this.msg = msg;
	}
	
	public String getRecipientUserName() {
		return recipient;
	}

	public void setRecipientUserName(String recipient) {
		this.recipient = recipient;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
