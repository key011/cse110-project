package edu.ucsd.cse110.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;

public class RegisterUser implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public void setUsername(String username) {
		this.username=username;
	}
	public void setPassword(String password) {
		this.password=password;
	}
	public String promptUsername() {
		String s=null;
		System.out.println("Please type in a valid username");
		BufferedReader commandLine = new java.io.BufferedReader(new InputStreamReader(System.in));
        // Loop until the word "exit" is typed
		try {       
            while(true){	
				 s = commandLine.readLine();
				 if (s.equals("exit"))
					 break;// exit program
				 }
		} catch (Exception e){ e.printStackTrace(); }
		this.setUsername(s);
		return username;
	}
	
	public String promptPassword() {
		String s=null;
		System.out.println("Please type in a valid password");
		BufferedReader commandLine = new java.io.BufferedReader(new InputStreamReader(System.in));
        // Loop until the word "exit" is typed
		try {       
            while(true){	
				 s = commandLine.readLine();
				 if (s.equals("exit"))
					 break;// exit program
				 }
		} catch (Exception e){ e.printStackTrace(); }
		this.setPassword(s);
		return password;
	}

}
