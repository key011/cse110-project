package edu.ucsd.cse110.client;



import java.io.Serializable;
import javax.jms.Queue;

public class RequestForRecipientQueue implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String recipientUserName;
	private String userMessage;
	
	
	public RequestForRecipientQueue(String recipientUserName) {
		this.recipientUserName = recipientUserName;
		userMessage = "";
	}
	
	public String getRecipientUserName() {
		return recipientUserName;
	}
	
	public void setRecipientUserName(String recipientUserName) {
		this.recipientUserName = recipientUserName;
	}
	
	public String getUserMessage() {
		return userMessage;
	}
	
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}
}