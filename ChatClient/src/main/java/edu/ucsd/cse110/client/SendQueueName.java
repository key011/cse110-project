package edu.ucsd.cse110.client;

import java.io.Serializable;

import javax.jms.Queue;

public class SendQueueName implements Serializable {
	private String userName;
	private Queue tempQueue;
	
	public SendQueueName(String userName, Queue tempQueue) {
		this.userName = userName;
		this.tempQueue = tempQueue;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Queue getTempQueue() {
		return tempQueue;
	}

	public void setTempQueue(Queue tempQueue) {
		this.tempQueue = tempQueue;
	}
}
