package edu.ucsd.cse110.client;



import java.net.URISyntaxException;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.jms.core.JmsTemplate;

//import edu.ucsd.cse110.server.ChatServerApplication;

public class ChatClientApplication {
/*
* This inner class is used to make sure we clean up when the client closes
*/

	static private class CloseHook extends Thread {
		ActiveMQConnection connection;
		private CloseHook(ActiveMQConnection connection) {
			this.connection = connection;
		}
		public static Thread registerCloseHook(ActiveMQConnection connection) {
			Thread ret = new CloseHook(connection);
			Runtime.getRuntime().addShutdownHook(ret);
			return ret;
		}
		
		public void run() {
			try {
				System.out.println("Closing ActiveMQ connection");
				connection.close();
			} catch (JMSException e) {
			/*
			* This means that the connection was already closed or got
			* some error while closing. Given that we are closing the
			* client we can safely ignore this.
			*/
			}
		}
	}
	
	
	
	
	/*
	* This method wires the client class to the messaging platform
	* Notice that ChatClient does not depend on ActiveMQ (the concrete
	* communication platform we use) but just in the standard JMS interface.
	*/
	
	private static ChatClient wireClient() throws JMSException,
	URISyntaxException {
		ActiveMQConnection connection =
		ActiveMQConnection.makeConnection(Constants.USERNAME, Constants.PASSWORD, Constants.ACTIVEMQ_URL);
		connection.start();
		CloseHook.registerCloseHook(connection);
		Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
		Queue destQueue = session.createQueue(Constants.QUEUENAME);
		Queue temporaryQueue = session.createTemporaryQueue();
		MessageProducer producer = session.createProducer(destQueue);
		MessageConsumer consumer = session.createConsumer(temporaryQueue);
		return new ChatClient(producer, consumer,session,temporaryQueue);
	} 
	
	public static void main(String[] args) {
	String username=null;
	String password=null;
	String recipientUserName = null; //get it from parsed input
	String userMessage = null;
	System.out.println("Please enter a command: (Login), (Sign up), or (Exit)");
	try {
		BufferedReader commandLine = new java.io.BufferedReader(new InputStreamReader(System.in));
		String s;
		while(true){
			s = commandLine.readLine();
			if (s.equalsIgnoreCase("exit")){
				System.exit(0);// exit program
			} 
			else {  
				if(s.equals("Login"))
				{
					ChatClient client = wireClient();
					System.out.println("Please enter your username");
					username=commandLine.readLine();
					SendQueueName queuename=new SendQueueName(username, client.getTemporaryQueue());
					client.send(queuename);
					
					//sending message to a user
					SendToUser stu = new SendToUser("Kevin", "fuck this");
					client.send(stu);
					//to see if we receive messages
					while(true) {
						TextMessage receivedMsg = (TextMessage) client.getConsumer().receive();
						String receivedString = receivedMsg.getText();
						System.out.println(receivedString);
						break;
					}
					
					password=commandLine.readLine();
					break;
				}
				else if(s.equals("Sign up"))
				{
					while(true)
					{
						System.out.println("Please enter a valid username (more than 5 characters, less than 20 characters)");
						username=commandLine.readLine();
						if(username.length()<5||username.length()>20)
							System.out.println("Invalid username!"); 
						else 
							break;
					}
					while(true)
					{
						System.out.println("Please enter a valid password (more than 7 characters, less than 20 characters)");
						password=commandLine.readLine();
						if(password.length()<7||password.length()>20)
							System.out.println("Invalid password!");
						break;
					}
					break;
				}
				else
				{
					System.out.println("Please enter a valid command! (Login), (Sign up), or (Exit)");
					continue;
				}
			}
		}

	} catch (Exception e){ e.printStackTrace(); }
	//We have some other function wire the ChatClient
	//to the communication platform

	//System.out.println("ChatClient wired.");
	//Now we can happily send messages around
	BufferedReader commandLine = new
	java.io.BufferedReader(new InputStreamReader(System.in));
	// Loop until the word "exit" is typed
	try {
		while(true){
			System.out.println("Please enter a command: (Message), (Add friend), (View friendlist), (Logout)");
			String s;
			s = commandLine.readLine();
			
			if (s.equalsIgnoreCase("logout")){
				System.exit(0);// exit program
			} 
			else {  //begin to parse command, multiple if statements looking for
				//specific command prompts like "send" or "add friend" etc
				//sending message to a specific user
				if(s.equals("Message"))
				{
					System.out.println("Please enter the username of the user you would like to message");
					recipientUserName=commandLine.readLine();
					RequestForRecipientQueue rfrq = new RequestForRecipientQueue(recipientUserName);
					//client.send(rfrq);
					//ChatClient client2 = wireClient(Constants.QUEUENAME+client.getRecipientQueue().getqueueName());
					while(true)
					{
						s=commandLine.readLine();
						//client2.send(s);
					}
				}
			} 
		} 

	} catch (Exception e){ e.printStackTrace(); } 
	System.exit(0);
	} 
}
